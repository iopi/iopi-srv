# Installation

Eine aktuelle Instanz des IoPi-Servers ist unter https://iopi.informatik.hu-berlin.de öffentlich verfügbar. Da die Daten, die an den Server geschickt und auf ihm gespeichert werden, in der Regel nicht sehr sensibel sind, kann diese Instanz genutzt werden, sofern die verwendeten Endgeräte mit dem Internet verbunden sind. Sollten die Geräte (z.B aus dem Schulnetzwerk heraus) keinen Internetzugang haben, kann ein Server im lokalen Netz aufgesetzt werden.

## 1 Das Notwendigste

IoPi ist als Docker-Image auf DockerHub verfügbar und kann darüber sehr leicht aufgesetzt werden. Docker ist eine schlanke Virtualisierungsumgebung, die es erlaubt, Anwendungen plattformunabhängig zu entwickeln und auszuliefern. Docker ist kostenfrei für alle gängigen Betriebssysteme verfügbar: https://www.docker.com/community-edition Eine ausführliche Dokumentation aller im Folgenden verwendeten Befehle finden Sie unter https://docs.docker.com/engine/reference/run/

Nach der Installation von Docker können Sie die gewünschte IoPi-Version wie folgt starten:

    docker run -it -p 80:9000 cses/iopi-srv:1.4
    
Docker wird daraufhin automatisch die angegebene Version (hier 1.4) von DockerHub herunterladen, starten und in diesem Beispiel an den Port 80 des Host-Rechners hängen. Danach ist der Server über den Hostnamen des Rechners unter diesem Port erreichbar. 

Um den Server als Hintergrundprozess zu starten, ersetzen Sie den Parameter ``-it`` (interactive terminal) durch ``-d`` (detached). Docker gibt daraufhin die ID des Prozesses auf der Kommandozeile aus. Um einen im Hintergrund laufenden Container wieder zu beenden, führen sie

    docker stop <ID>
    
aus. Für einmalige oder kurzzeitige Einsätze des Servers reicht dieses Setup vermutlich aus. Soll der Server über längere Zeit betrieben werden, können noch je nach Bedarf die im Folgenden beschriebenen Konfigurationen vorgenommen werden.

## 2 Optionale Konfigurationen

### Die Datenbank persistent machen

Standardmäßig läuft die Datenbank des Servers vollständig innerhalb des Docker-Containers. Bei einem Neustart des Containers gehen die Daten daher ggf. verloren. Um die Datenbank auch über Container (und Versions-Updates!) hinweg persistent zu machen, muss sie in das Dateisystem des Host-Rechners ausgelagert werden. Dies geht am Einfachsten über ein Docker-Volume. Fügen Sie dazu beim Start des Containers den Parameter

    -v iopidb:/data/db

hinzu. Dies weist Docker an, das Container-Verzeichnis /data/db (in dem sich die Datenbank befindet) in ein Docker-Volume mit der Bezeichnung iopidb auszulagern. Existiert ein solches Volume noch nicht, erzeugt Docker es automatisch. Bei einem späteren späteren Start eines Containers kann das Volume dann über denselben Parameter wieder eingehangen werden, sodass der Container die die Datenbank darin vorfindet.

### Log-Dateien auslagern

Um die Log-Dateien des Servers für den Host-Rechner sichtbar zu machen, fügen sie folgenden Parameter beim Start des Containers hinzu:

    -v /path/to/logs:/opt/iopi/logs
    
Dies weist Docker an, das Container-Verzeichnis /opt/iopi/logs (in dem sich die Log-Dateien befinden) in ein Verzeichnis des Host-Dateisystems auszulagern. Das Verzeichnis kann frei gewählt werden. Sollte es noch nicht existieren, versucht Docker automatisch, es anzulegen.

### SSL

#### Selbst signierte Zertifikate

Der IoPi-Server generiert beim Start automatisch Zertifikate für die Kommunikation via HTTPS. Um den Server darüber erreichbar zu machen, muss lediglich der entsprechende Port and den Host gebunden werden. Fügen Sie dazu beim Start des Containers den folgenden Parameter hinzu:

    -p 443:9443

Danach ist der Server auf Port 443 des Hostrechners über HTTPS erreichbar. In nahezu allen Browsern verursacht ein Aufruf der Webseite jedoch zunächst eine Sicherheitswarnung, da das vom Server selbst signierte Zertifikat nicht als Vertrauenswürdig eingestuft wird. 

#### CA Zertifikate

Coming soon.

## 3 Upgrades

Coming soon.