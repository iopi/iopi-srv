import com.typesafe.config._
import com.typesafe.sbt.packager.docker._

name		:= """iopi-srv"""
maintainer 	:= "Michael T. Rücker (rueckerm@informatik.hu-berlin.de)"

val appConf = ConfigFactory.parseFile(new File("conf/application.conf")).resolve()

version	:= 
	appConf.getString("app.version_major")+"."+
	appConf.getString("app.version_minor")+
	appConf.getString("app.version_suffix")
	

lazy val root = (project in file(".")).enablePlugins(PlayJava, DockerPlugin)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "org.mongodb.morphia" % "morphia" % "1.2.1"
)

// Eclipse
// -------
EclipseKeys.projectFlavor := EclipseProjectFlavor.Java       							
EclipseKeys.createSrc := EclipseCreateSrc.ValueSet(
	EclipseCreateSrc.ManagedClasses, 
	EclipseCreateSrc.ManagedResources)	

// Docker
// ------
maintainer in Docker := "Michael T. Rücker (rueckerm@informatik.hu-berlin.de)"

dockerBaseImage := "alpine:3.7"

daemonUser in Docker := "root"
defaultLinuxInstallLocation in Docker := "/opt/iopi"

dockerExposedVolumes := Seq(
	"/data/db", 
	s"""${(defaultLinuxInstallLocation in Docker).value}/logs""",
	s"""${(defaultLinuxInstallLocation in Docker).value}/ssl""")
	
dockerCommands := dockerCommands.value.flatMap {
	case cmd@Cmd("FROM", _) => List(cmd, 
		Cmd("RUN", "apk update"),
		Cmd("RUN", "apk upgrade"),
		Cmd("RUN", "apk add --no-cache"
		 	+ " bash"
		 	+ " mongodb "
		 	+ " openjdk8-jre-base"
		 	+ " openrc"
		 	+ " tzdata"),
		Cmd("RUN", "rm -rf /var/cache/apk/"))
		
	case other => List(other)
}

dockerExposedPorts := Seq(9000, 9443)

dockerEntrypoint := Seq("conf/docker-launcher.sh")
