#!/bin/bash

echo $1

# Check if mongodb or IoPi are already running.
[ -f "RUNNING_PID" ] && !(kill `cat RUNNING_PID`) && rm "RUNNING_PID"	
	
# Start mongodb:
echo "[info] launcher - Starting MongoDB"
exec mongod > logs/mongodb.log &

PARAM="-Dhttp.port=9000 -Dhttps.port=9443"

KEYSTORE_PATH="/opt/iopi/ssl/iopi.jks"

# Check if keystore exists:
if [ -f $KEYSTORE_PATH ]; then
	echo "[info] launcher - Keystore found."
	PARAM="${PARAM} -Dplay.server.https.keyStore.path=${KEYSTORE_PATH}"
else
	echo "[info] launcher - No keystore found: using self-signed certificate."
fi

# Start Play! application:
echo "[info] launcher - Starting IoPi with Play! options: ${PARAM} ${1}"
eval "bin/iopi-srv ${PARAM} ${1}" 