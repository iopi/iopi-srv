package actors;

import akka.actor.UntypedActor;
import controllers.ProjectController;
import play.Logger;

public class ScheduledActor extends UntypedActor {

	@Override
	public void onReceive(Object message) throws Throwable {

		if(message instanceof String) {
			String strMessage = (String)message;
		
			switch(strMessage) {
			
			case "projects: clean abandoned":
				ProjectController.cleanAbandoned();
				break;
			
			case "websockets: check heartbeats":
				ProjectController.publisher.checkHeartbeats();
				break;
				
			default:
				Logger.warn("ScheduledActor received undefined message: {}", strMessage);
				break;
			}
		
		} else {
			Logger.warn("ScheduledActor received non-String message: {}", message);
		}
	}

}