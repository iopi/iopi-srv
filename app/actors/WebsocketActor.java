package actors;

import akka.actor.*;
import akka.actor.Status.Failure;
import controllers.*;
import models.Project;
import play.Logger;
import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static controllers.ProjectController.publisher;

public class WebsocketActor extends UntypedActor {

	@Override
	public void onReceive(Object message) throws Throwable {
		
		if (message instanceof JsonNode) {
            JsonNode jsonMessage = (JsonNode) message;
            String event_type = jsonMessage.get("event_type").textValue();
            JsonNode data = jsonMessage.get("data");
            
            switch(event_type) {
            
	            case "system":
	        		systemEvent(data);
	        		break;	
            
            	case "user":
            		userEvent(data);
            		break;
            		
            	case "chartdata":
            		datasetEvent(data);
            		break;
            		
            	case "command":
            		commandEvent(data);
            		break;
            		
            	default:
            		Logger.warn("Received unspecified websocket event type: {}", event_type);
            }

        } else if(message instanceof Failure) {
        	Logger.warn("Failure caught: {}", message.toString());
        
        } else if (message.toString() != "success" ) {
            Logger.warn("WebSocketActor received unspecified non-Json: {}", message.toString());
        }
	}
	
	private void userEvent(JsonNode data) {
		String action = data.get("action").asText();
		
		switch(action) {
		
			case "joined":
				Project p = ProjectController.getProject(data.get("project").asText());
				ProjectController.userJoined(p);
				break;
			
			default:
				Logger.warn("WebsocketActor received unspecified user action: {}", action);
		}
	}
	
	private void systemEvent(JsonNode data) {
		String action = data.get("action").asText();
		
		switch(action) {
		
			case "ping":
				String uid = data.get("uid").asText();
				publisher.updateHeartBeat(uid);
				
				ObjectNode node = Json.newObject();
				node.put("event_type", "system");
					ObjectNode d = Json.newObject();
					d.put("action", "pong");
				node.set("data", d);
				publisher.tell(uid, node);
				break;
				
			default:
				Logger.warn("WebsocketActor received unspecified system action: {}", action);
		}
	}
	
	private void datasetEvent(JsonNode data) {
		String action = data.get("action").asText();
		
		switch(action) {
		
			case "delete_set":
				Project p = ProjectController.getProject(data.get("project").asText());
				String ds = data.get("setname").asText();
            	SensorDataController.deleteDataSet(p, ds);
            	break;
            	
        	default:
        		Logger.warn("WebsocketActor received unspecified dataset action: {}", action);
		}
	}
	
	private void commandEvent(JsonNode data) {
		String action = data.get("action").asText();
		Project p = ProjectController.getProject(data.get("project").asText());
		
		switch(action) {
		
			case "clear_queue":
				CommandController.clearQueue(p);
				break;
				
			case "issue":
				String cmd = data.get("command").asText();
            	CommandController.queueCmd(p, cmd);
				break;
				
			default:
        		Logger.warn("WebsocketActor received unspecified command action: {}", action);
		}
	}
}
