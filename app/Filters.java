import javax.inject.*;
import play.mvc.EssentialFilter;
import play.http.HttpFilters;

import filters.HttpsRedirectFilter;

/**
 * This class configures filters that run on every request. This
 * class is queried by Play to get a list of filters.
 *
 * Play will automatically use filters from any class called
 * <code>Filters</code> that is placed the root package. You can load filters
 * from a different class by adding a `play.http.filters` setting to
 * the <code>application.conf</code> configuration file.
 */
@Singleton
public class Filters implements HttpFilters {

    private final EssentialFilter httpsRedirectFilter;

    /**
     * @param env Basic environment settings for the current application.
     * @param httpsRedirectFilter A demonstration filter that adds a header to
     */
    @Inject
    public Filters(HttpsRedirectFilter httpsRedirectFilter) {
        this.httpsRedirectFilter = httpsRedirectFilter;
    }

    @Override
    public EssentialFilter[] filters() {
      return new EssentialFilter[] { httpsRedirectFilter };
    }

}
