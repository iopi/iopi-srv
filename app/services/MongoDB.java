package services;

import com.mongodb.MongoClient;
import org.mongodb.morphia.*;
import com.typesafe.config.ConfigFactory;

public class MongoDB {
	 private static AdvancedDatastore s_mongoDB;

	    public static AdvancedDatastore db() {
	        if (s_mongoDB == null) {
	            initMongoDB();
	        }
	        return s_mongoDB;
	    }

	    public static void initMongoDB() {

	        final Morphia morphia = new Morphia();

	        morphia.getMapper().getOptions().setStoreEmpties(true);
	        morphia.mapPackage("models");

	        MongoClient mongoClient = new MongoClient(
	        		ConfigFactory.load().getString("mongodb.host"),
	        		ConfigFactory.load().getInt("mongodb.port"));	        
	        
	        s_mongoDB = (AdvancedDatastore) morphia.createDatastore(
	        		mongoClient, ConfigFactory.load().getString("mongodb.database"));
	    }
}