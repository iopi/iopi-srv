package services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Source;
import controllers.ProjectController;
import models.Project;
import play.Logger;

public class WebSocketManager<T> {

	// Keep a map between project IDs and ActorRefs so that we can broadcast changes to all sockets of a project
	private final HashMap<String, HashSet<String>> projectUsers = new HashMap<String, HashSet<String>>();
	private final HashMap<String, UserRef> userMap = new HashMap<String, UserRef>();
	
	public Source<T, ?> register(String uid, Project p) {
		
		if(!accept(uid)) {
			Logger.error("Unacceptable uID: {} - CANCEL REGISTER", uid);
			return Source.empty();
		}
		
		String pID = p.id.toHexString();
		
		// Register the user id to the project
		if(this.projectUsers.containsKey(pID)) {
			if(this.projectUsers.get(pID).contains(uid)) {
				Logger.warn("User {} already registered to proejct {}", uid, pID);
			} else {
				this.projectUsers.get(pID).add(uid);
			}
		} else {
			HashSet<String> uidSet = new HashSet<String>();
			uidSet.add(uid);
			this.projectUsers.put(pID, uidSet);
		}

		// Register the akka.ActorRef to the user id
        Source<T, ?> source = Source.<T>actorRef(256, OverflowStrategy.dropHead())
              
        		.mapMaterializedValue(actorRef -> {

        			UserRef user = new UserRef(uid, pID, actorRef);
            		WebSocketManager.this.userMap.put(uid, user);
                	ProjectController.userJoined(p);
                    return actorRef;
                
        		})
                
                .watchTermination((actorRef, termination) -> {
                    termination.whenComplete((done, cause) -> {
                    	
                	WebSocketManager.this.unregister(uid);
                	ProjectController.userLeft(p);
                	
                    } );
                    return null;
                });
        
        return source;
    }

	public boolean accept(String uID) {
		return !(this.userMap.containsKey(uID));
	}
	
	public void tell(String uid, final T message) {
		if(!userMap.containsKey(uid)) return;
		userMap.get(uid).actorRef.tell(message, ActorRef.noSender());
	}
	
    public void broadcast(Project p, final T message) {
    	
    	String pID = p.id.toHexString();

    	HashSet<String> userSet = projectUsers.get(pID);
    	if(userSet == null || userSet.isEmpty()) return;
    	
    	Iterator<String> it = userSet.iterator();
    	while(it.hasNext()) {
    		userMap.get(it.next()).actorRef.tell(message, ActorRef.noSender());
    	}
    }
    
    public int getNumConnections(Project p) {
    	String pID = p.id.toHexString();
    	
    	if(!this.projectUsers.containsKey(pID))
    		return 0;
    	
    	return this.projectUsers.get(pID).size();
    }
    
    public void checkHeartbeats() {
    	
    	ArrayList<String> timedOut = new ArrayList<String>();
    	
    	Iterator<Map.Entry<String,UserRef>> users = userMap.entrySet().iterator();
    	while(users.hasNext()) {
    		Map.Entry<String, UserRef> u = users.next();
    		if( !u.getValue().isAlive() ) {
    			timedOut.add(u.getKey());
    		}
    	}
    	
    	Iterator<String> it = timedOut.iterator();
    	while(it.hasNext()) {
    		UserRef user = this.userMap.get(it.next());
    		Logger.debug("Timeout: {}", user.uID);
    		user.actorRef.tell(PoisonPill.getInstance(), ActorRef.noSender());
    	}
    }
    
    public void updateHeartBeat(String uid) {
    	UserRef u = this.userMap.get(uid);
    	if(u != null) u.updateHeartBeat();
    	else Logger.warn("Failed to update heartbeat for user {}: ID not found", uid);
    }
    
    private void unregister(String uid) {

    	UserRef u = this.userMap.remove(uid);
    	
    	if(u == null) {
    		Logger.warn("Failed to unregister user {}: ID not found", uid);
    	
    	} else if(!this.projectUsers.get(u.pID).remove(uid)) {
    		Logger.warn("Failed to unregister user {} from proejct {}", uid, u.pID);
    	
    	} else {
    		Logger.debug("Unregistered user {}", uid);
    	}
    }
}
