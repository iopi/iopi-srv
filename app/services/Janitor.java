package services;

import javax.inject.Named;
import javax.inject.Inject;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import controllers.ProjectController;
import models.Project;
import models.ServerInfo;
import play.Logger;
import play.inject.ApplicationLifecycle;
import scala.concurrent.duration.Duration;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class Janitor {
	
	private ServerInfo currInstance;
	
	@Inject
    public Janitor(final ActorSystem system, @Named("scheduled-actor") ActorRef scheduledActor, ApplicationLifecycle appLifeCycle) {
		
		currInstance = new ServerInfo();
		
		// See what we are up against
		checkIntegrity();
		
		// Schedule periodic tasks
		scheduleTasks(system, scheduledActor);
        
		// Register stop hooks
		registerStopHooks(appLifeCycle);
		
		// Write current instance info to database
		MongoDB.db().getDB().getCollection("iopi_info").drop();
		MongoDB.db().save("iopi_info", currInstance);
	}
	
	private void checkIntegrity() {
		
		Logger.info("Running database integrity checks.");
		
		ServerInfo prevInstance = null;
		long prevTimestamp = 0;
		
		List<ServerInfo> infoList = MongoDB.db().find("iopi_info", ServerInfo.class).asList();
		
		// There is info on a previous IoPi instance in the database
		if(!infoList.isEmpty()) {

			if(infoList.size() > 1) {
				Logger.warn("Found info on multiple previous IoPi instances in the database. This should not happen! Using the most recent one.");
			}
			
			Iterator<ServerInfo> it = infoList.iterator();
			while(it.hasNext()) {
				ServerInfo si = it.next();
				if(si.started > prevTimestamp) {
					prevInstance 	= si;
					prevTimestamp 	= si.started;
				}
			}
			
			// The previous instance was a newer version
			if(currInstance.compareVersions(prevInstance) < 0) {
				Logger.error("Database was last used by a more recent version of IoPi. "
						+ "Drop the database or upgrade to IoPi version {} or later.", prevInstance.getVersionString());
				System.exit(1);
				
	
			// The previous instance was an older version
			} else if(currInstance.compareVersions(prevInstance) > 0) {
				Logger.info("Database was last used by an older IoPi version: {}.", prevInstance.getVersionString());
				migrateDatabase(prevInstance);
			} 
		

		// There is no info on a previous IoPi instance, but there are projects in the database
		} else if(ProjectController.getNumProjects() > 0) {
			migrateDatabase(new ServerInfo(1, 3, ""));
		}
		
		Logger.info("Integrity checks complete.");
	}
	
	private void scheduleTasks(final ActorSystem system, @Named("scheduled-actor") ActorRef scheduledActor) {
	
		system.scheduler().schedule(
            Duration.create(60, TimeUnit.SECONDS), 
            Duration.create(1, TimeUnit.DAYS), 
            scheduledActor,
            "projects: clean abandoned",
            system.dispatcher(),
            null);
		
		system.scheduler().schedule(
			Duration.create(10, TimeUnit.SECONDS),
			Duration.create(10, TimeUnit.SECONDS),
			scheduledActor,
			"websockets: check heartbeats",
			system.dispatcher(),
			null);
	}
	
	private void registerStopHooks(ApplicationLifecycle appLifeCycle) {
		appLifeCycle.addStopHook(() -> {
			Logger.info("Application stopped.");
			return CompletableFuture.completedFuture(null);
		});
	}
	
	private void migrateDatabase(ServerInfo prevInstance) {
		
		// No migration necessary prior to 1.3
		while(prevInstance.compareVersions(1, 3) < 0)
			prevInstance.nextVersion();

		// Migrate database from 1.3 to 1.4
		if(prevInstance.compareVersions(1, 4) < 0) {
			Logger.info("Migrating database to version 1.4");
			List<Project> pl = ProjectController.getProjects();
			Iterator<Project> pit = pl.iterator();
			while(pit.hasNext()) {
				Project p = pit.next();
				p.last_accessed = System.currentTimeMillis();
				p.update();
				Logger.debug("Migrating project: {}", p.name);
			}
			prevInstance.nextVersion();
		}

		if(prevInstance.compareVersions(currInstance) != 0) {
			Logger.error("Still detecting version mismatch after database migration. This is a bug. Please contact a developer.");
		}
	}
}
