package services;

import akka.actor.ActorRef;
import scala.concurrent.duration.Duration;
import java.util.concurrent.TimeUnit;


public class UserRef {
	
	private final static long timeOut = Duration.create(10, TimeUnit.SECONDS).toMillis();
	
	public String uID;
	public String pID;
	public long lastHeartBeat;
	public ActorRef actorRef;
	
	public UserRef(String uID, String pID, ActorRef actorRef) {
		this.uID = uID;
		this.pID = pID;
		this.actorRef = actorRef;
		this.lastHeartBeat = System.currentTimeMillis();
	}
	
	public void updateHeartBeat() {
		this.lastHeartBeat = System.currentTimeMillis();
	}
	
	public boolean isAlive() {
		long now = System.currentTimeMillis();
		return (now < (this.lastHeartBeat + timeOut));
	}
	
	public boolean equals(UserRef ur) {
		return this.equals(ur.uID);
	}
	
	public boolean equals(String uid) {
		return this.uID.equals(uid);
	}
}
