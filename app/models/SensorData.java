package models;

import com.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties({ "id", "type" })
@JsonPropertyOrder({ "x", "y" })
public class SensorData extends DataEntry {
	
	@JsonProperty("y")
	public double value;
}
