package models;

import java.util.Calendar;
import java.util.Locale;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import play.Logger;

@Entity(value = "server_info")
public class ServerInfo {
	
	@Id
	public ObjectId id;
	
	public int		version_major;
	public int		version_minor;
	public String	version_suffix;

	public String	imprint;
	
	public long		started;
	
	public ServerInfo(int major, int minor, String affix) {
		version_major = major;
		version_minor = minor;
		version_suffix = affix;
		started = 0;
	}
	
	public ServerInfo() {
		Config conf = ConfigFactory.load();
		
		// Version
		version_major = Integer.parseInt( conf.getString("app.version_major") );
		version_minor = Integer.parseInt( conf.getString("app.version_minor") );
		version_suffix = conf.getString("app.version_suffix");
		
		// Imprint
		if(conf.hasPath("app.imprint"))
			imprint = conf.getString("app.imprint");
		else
			imprint = null;
		
		// Start time
		started	= System.currentTimeMillis();
	}
	
	public String getVersionString() {
		return version_major + "." + version_minor + version_suffix;
	}
	
	public String getVersion() {
		return version_major + "." + version_minor;
	}
	
	public String getStartDateString() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(this.started);
		String display = cal.get(Calendar.DAY_OF_MONTH) + ". " +
				cal.getDisplayName(Calendar.MONTH, Calendar.LONG_FORMAT, Locale.GERMAN) + " " +
				cal.get(Calendar.YEAR);
		return display;
	}
	
	public String getImprint() {
		return imprint;
	}
	
	public String getStartTimeString() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(this.started);
		String display = "";
		
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if(hour < 10) display += "0";
		display += hour + ":";
		
		int minute = cal.get(Calendar.MINUTE);
		if(minute < 10) display += "0";
		display += minute + ":";
		
		int second = cal.get(Calendar.SECOND);
		if(second < 10) display += "0";
		display += second;
		
		return display;
	}
	
	public int compareVersions(ServerInfo si) {
		return compareVersions(si.version_major, si.version_minor);
	}
	
	public int compareVersions(int major, int minor) {
		int majorDiff = version_major - major;
		int minorDiff = version_minor - minor;
		if(majorDiff != 0) return majorDiff;
		else return minorDiff;
	}
	
	public boolean nextVersion() {
		final int[][] versionHistory = {
			{1,0},	
			{1,1},
			{1,2},
			{1,3},
			{1,4}
		};
		final int numVersions = 5;
		
		int i = 0;
		boolean updated = false;
		while(i++ < (numVersions-1) && !updated) {
			if(versionHistory[i][0] == version_major && versionHistory[i][1] == version_minor) {
				version_major = versionHistory[i+1][0];
				version_minor = versionHistory[i+1][1];
				updated = true;
			}
		}
		
		if(!updated && (versionHistory[numVersions-1][0] != version_major || versionHistory[numVersions-1][1] != version_minor)) {
			Logger.error("Version history mismatch for version" + getVersion());
		}
		
		return updated;
	}
}
