package models;

import java.util.ArrayList;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import play.Logger;
import services.MongoDB;

@Entity(value = "projects")
public class Project {
		
	@Id
	public ObjectId id;
	
	public String name;
	
	public long time_created;
	public long last_accessed;
	
	public ArrayList<String> data_sets;	
	
	public Project() {
		data_sets = new ArrayList<String>();
		time_created = System.currentTimeMillis();
		last_accessed = time_created;
	}
	
	public void save() {
		MongoDB.db().save(this);
	}
	
	public void update() {
		
		Query<Project> query = MongoDB.db().createQuery("projects", Project.class).field("_id").equal(this.id);
		
		UpdateOperations<Project> ops = MongoDB.db().createUpdateOperations(Project.class).set("last_accessed", this.last_accessed);
		if( !MongoDB.db().update(query, ops).getUpdatedExisting() ) Logger.error("Failed to update Project access time.");
		
		ops = MongoDB.db().createUpdateOperations(Project.class).set("data_sets", this.data_sets);
		if( !MongoDB.db().update(query, ops).getUpdatedExisting() ) Logger.error("Failed to update Project data sets.");
	}
}
