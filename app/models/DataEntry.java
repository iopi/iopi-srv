package models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public abstract class DataEntry {

	@Id	
	public ObjectId		id;
	
	@JsonProperty("x")
	public long			timestamp;
	
	public DataEntry() {
		timestamp 	= System.currentTimeMillis();
	}
}
