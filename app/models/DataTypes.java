package models;

public enum DataTypes {
	DATA_TYPE_INVALID,
	DATA_TYPE_EVENT_LOG,
	DATA_TYPE_SENSOR_LOG
}
