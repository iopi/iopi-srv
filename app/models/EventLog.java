package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({ "id", "type" })
public class EventLog extends DataEntry {

	public EventTypes event_type;
	public String log_message;
}
