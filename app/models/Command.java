package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({ "id", "type" })
public class Command extends DataEntry {

	public String command;
}
