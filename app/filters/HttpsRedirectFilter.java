package filters;

import akka.stream.Materializer;

import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import javax.inject.*;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import play.Logger;
import play.mvc.*;
import play.mvc.Http.RequestHeader;


@Singleton
public class HttpsRedirectFilter extends Filter {

	boolean redirectEnabled;
	boolean httpsEnabled;
	int		httpsPort;
	
    @Inject
    public HttpsRedirectFilter(Materializer mat) {
        super(mat);
        
        Config conf 	= ConfigFactory.load();
        redirectEnabled = conf.hasPath("https.redirect");
        httpsEnabled 	= conf.hasPath("https.port");
        if(httpsEnabled && redirectEnabled) 
        	httpsPort 	= Integer.parseInt( conf.getString("https.redirect") );
    }
    
    @Override
    public CompletionStage<Result> apply(
        Function<RequestHeader, CompletionStage<Result>> next,
        RequestHeader requestHeader) {

    	CompletionStage<Result> csRes = next.apply(requestHeader);
    	
    	boolean isDeviceRequest = 
    			requestHeader.path().contains("/msg/") || 
    			requestHeader.path().contains("/dat/") || 
    			requestHeader.path().contains("/cmd/");
    	
    	if(!isDeviceRequest && redirectEnabled && !requestHeader.secure() && httpsEnabled) {
    		csRes = csRes.thenApply(result -> 
    			Results.redirect(createHttpsRedirectURL(requestHeader))
			);
    	} 
		
    	return csRes;
    }

    private String createHttpsRedirectURL(RequestHeader requestHeader) {
    	
    	String url = "https://";
    	url += requestHeader.host().split(":")[0];
    	if(httpsPort != 443) 
    		url += ":" + httpsPort;
    	url += requestHeader.uri();
    	
    	Logger.debug("Redirecting HTTP to HTTPS: {}", url);
    	
    	return url;
    }
}
