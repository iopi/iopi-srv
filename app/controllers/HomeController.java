package controllers;

import play.data.DynamicForm;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import models.ServerInfo;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;
import play.twirl.api.Html;
import services.MongoDB;
import views.html.*;

public class HomeController extends Controller {

	public Result aboutPage() {
		
		// Build JSON
		ServerInfo srvInfo = MongoDB.db().find("iopi_info", ServerInfo.class).get();
		String srvVersion = srvInfo.getVersionString();
		String srvStartDate = srvInfo.getStartDateString();
		String srvStartTime = srvInfo.getStartTimeString() + " Uhr";
		String srvImprint = srvInfo.getImprint();
		int numProjects = ProjectController.getNumProjects();
		
		
		
		ObjectNode srvStats = Json.newObject();
		srvStats.put("version", srvVersion);
		srvStats.put("startdate", srvStartDate);
		srvStats.put("starttime", srvStartTime);
		srvStats.put("projects", numProjects);
		if(srvImprint != null) srvStats.put("imprint", srvImprint);
		
		return ok(aboutView.render(new Html(Json.stringify(srvStats))));
	}
	
    public Result loginPage() {
    	return ok(loginView.render());
    }
    
    @Inject
    private FormFactory formFactory;
    
    public Result submit() {
    	
    	// Retrieve project ID from HTML form
    	DynamicForm form = formFactory.form().bindFromRequest();
    	String pName = form.get("pName");
    	
    	if(pName != null) {
    		return redirect(routes.ProjectController.index(pName));
    	
    	} else {
    		return badRequest("Not a valid login submission.");
    	}
    }
}
