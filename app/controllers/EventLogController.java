package controllers;

import services.*;
import models.*;
import play.libs.Json;
import play.mvc.*;

import static controllers.ProjectController.publisher;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class EventLogController extends Controller {
	
	public Result logUserEvent(String pName, String logMsg) {
		Project p = ProjectController.getProject(pName);
		return logEvent(p, logMsg, EventTypes.EVENT_TYPE_USER);
	}
	
	public static Result logEvent(Project p, String logMsg, EventTypes eventType) {
		
		String collectionName = p.id.toHexString()+".eventLogs";
		EventLog eventLog = new EventLog();
		eventLog.log_message = logMsg;	
		eventLog.event_type = eventType;
				
		if(MongoDB.db().save(collectionName, eventLog) != null) {
			
			ObjectNode node = Json.newObject();
			node.put("event_type", "eventlog");
			node.set("data", Json.toJson(eventLog));
			publisher.broadcast(p, node);
			return ok();
		}
			
		else 
			return internalServerError("Saving event log to database failed.");
	}	
	
	public static JsonNode getJsonEventLogs(Project p) {
		List<DataEntry> eventLogs = MongoDB.db().find(p.id.toHexString()+".eventLogs", DataEntry.class).asList();		
		return Json.toJson(eventLogs);
	}
}
