package controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.mongodb.morphia.query.Query;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import actors.*;
import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import models.*;
import play.Logger;
import play.libs.F;
import play.libs.Json;
import play.mvc.*;
import play.twirl.api.Html;
import scala.concurrent.duration.Duration;
import services.MongoDB;
import services.WebSocketManager;
import views.html.*;

public class ProjectController extends Controller {

	private  ActorSystem actorSystem = ActorSystem.create();
	
	private ActorRef websocketActor = actorSystem.actorOf(Props.create(WebsocketActor.class));

    public static final WebSocketManager<JsonNode> publisher = new WebSocketManager<>();
	
	/**
	 * Method to handle HTTP requests. Renders the page of project
	 * @param pName the name of the project
	 * @return an action result
	 */
	public Result index(String pName) {

		if(!isValidID(pName)) {
			return redirect( routes.HomeController.loginPage() );
		}
		
		// Get project, logs and data
		Project p = getProject(pName);
		Html htmlEventLogs = new Html(EventLogController.getJsonEventLogs(p).toString());
		Html htmlSensorData = new Html(SensorDataController.getJsonChartData(p).toString());		
		
		// Render the view
		return ok(projectView.render(p, htmlEventLogs, htmlSensorData));
	}
	
	/**
	 * Method to handle WebSocket requests. Creates a WebSocket and registers it
	 * @return the WebSocket instance
	 */
	public WebSocket ws(String pName, String uid) {
				
		Project p = ProjectController.getProject(pName);
		
		return WebSocket.Json.acceptOrResult(requestHeader -> {
			
			if(publisher.accept(uid)) {
				
				Logger.debug("WS Request from {} accepted.", uid);

				Sink<JsonNode, NotUsed> sink = Sink.actorRef(websocketActor, "success");
				Source<JsonNode, ?> source = publisher.register(uid, p);
	            Flow<JsonNode, JsonNode, NotUsed> flow = Flow.fromSinkAndSource(sink, source);
	            return CompletableFuture.completedFuture(F.Either.Right(flow));
			
			} else {
				
				Logger.debug("WS Request from {} rejected.", uid);
				
				return CompletableFuture.completedFuture(F.Either.Left(Results.forbidden("ID already registered.")));
			}
		});
    }
	
	/**
	 * Retrieves project of the given name from database and returns an instance of it. If no project with that
	 * name is found, it will be created. Changes to the returned instance will not affect the database.
	 * @param pName name of the project
	 * @return Project reference
	 */
	public static Project getProject(String pName) {
				
		Project p = null; 
		List<Project> lp = MongoDB.db().find("projects", Project.class, "name", pName, 0, 2).asList(); 
    	
		// Project does not exist, so create it
    	if(lp.isEmpty()) {	
    		p = new Project();
    		p.name = pName;
    		p.save();
    		EventLogController.logEvent(p, "Projekt erstellt.", EventTypes.EVENT_TYPE_SYSTEM);
    	
    	// Project exists, so update its access time stamp
    	} else {
    		p = lp.get(0);
			p.last_accessed = System.currentTimeMillis();
    		p.update();
    	}
    	
    	if(lp.size() > 1) {
    		Logger.error("Multiple projects with the same name found: {}; using first instance", pName);    		
    	}
    	
    	return p;    	
	}
	
	public static List<Project> getProjects() {
		return MongoDB.db().find("projects", Project.class).asList();
	}
	
	public static int getNumProjects() {
		return getProjects().size();
	}
	

	public static int getActiveUsers(Project p) {
		return publisher.getNumConnections(p);
	}
	
	public static void userJoined(Project p) {
		ObjectNode node = Json.newObject();
		node.put("event_type", "user");
		
		ObjectNode data = Json.newObject();
		data.put("action", "joined");
		data.put("number", getActiveUsers(p));

		node.set("data", data);
		
		publisher.broadcast(p, node);
	}
	
	public static void userLeft(String pID) {
		Logger.debug("_A: {}", pID);
		Project p = getProject(pID);
		Logger.debug("_B: {}", p);
		userLeft(p);
		Logger.debug("_SUCCESS_");
	}
	
	public static void userLeft(Project p) {
		ObjectNode node = Json.newObject();
		node.put("event_type", "user");
		
		ObjectNode data = Json.newObject();
		data.put("action", "left");
		data.put("number", getActiveUsers(p));

		node.set("data", data);
		
		publisher.broadcast(p, node);
	}
	
	public static boolean isValidID(String pName) {
		return pName.matches("[\\p{Alnum}[-_ ]]+");
	}
	
	public static void cleanAbandoned() {
		
		ArrayList<Project> toDelete = new ArrayList<Project>();
		
		List<Project> pList = getProjects();
		Iterator<Project> pit = pList.iterator();
		while(pit.hasNext()) {
			Project p = pit.next();
			long now = Calendar.getInstance().getTimeInMillis();
			long maxInactive = Duration.create(90, TimeUnit.DAYS).toMillis();
			if(now > p.last_accessed+maxInactive) toDelete.add(p);
		}
		
		if(!toDelete.isEmpty()) 
			Logger.info("Deleting " + toDelete.size() + " abandoned project(s).");
		
		pit = toDelete.iterator();
		while(pit.hasNext()) {
			deleteProject(pit.next());
		}
	}
	
	private static void deleteProject(Project p) {
		Logger.info("Deleting project: "+p.name);
		String pid = p.id.toHexString();
		
		// Drop eventlogs collection
		MongoDB.db().getDB().getCollectionFromString(pid+".eventLogs").drop();
		
		// Drop commands collection
		MongoDB.db().getDB().getCollectionFromString(pid+".commands").drop();

		// Dropt data set collections
		Iterator<String> dsit = p.data_sets.iterator();
		while(dsit.hasNext()) {
			MongoDB.db().getDB().getCollectionFromString(pid+".data."+dsit.next()).drop();
		}
		
		// Delete project document
		Query<Project> query = MongoDB.db().createQuery("projects", Project.class).field("_id").equal(p.id);
		MongoDB.db().delete(query);
	}
}
