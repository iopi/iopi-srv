package controllers;

import java.util.List;

import com.google.inject.Inject;

import models.*;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.*;
import services.MongoDB;

public class CommandController extends Controller {

	@Inject
    private FormFactory formFactory;
	
	public Result submitCmd() {
		// Retrieve project ID from HTML form
    	DynamicForm form = formFactory.form().bindFromRequest();
    	String pName = form.get("pName");
    	String strCmd = form.get("command");
    	
    	if(pName != null && strCmd != null) {
    		Project p = ProjectController.getProject(pName);
    		queueCmd(p, strCmd);
    		return redirect(routes.ProjectController.index(pName));
    	
    	} else {
    		return badRequest("Not a valid command submission.");
    	}
	}
	
	public Result forwardCmd(String pName) {
		Project p = ProjectController.getProject(pName);
		String strCollection = p.id.toHexString() + ".commands";
		List<Command> cmdQueue = MongoDB.db().find(strCollection, Command.class).asList();
		
		if(cmdQueue.isEmpty()) {
			return ok();
		
		} else {
			Command c = cmdQueue.get(0);
			MongoDB.db().delete(strCollection, Command.class, c.id);
			EventLogController.logEvent(p, "Befehl ["+c.command+"] erhalten.", EventTypes.EVENT_TYPE_COMMAND_QUEUE_OUT);
			return ok(c.command);		
		}
	}
	
	public static void queueCmd(Project p, String strCmd) {
		Command cmd = new Command();
    	cmd.command = strCmd;
    	
    	String strCollection = p.id.toHexString()+".commands"; 
    	    	    	
    	MongoDB.db().save(strCollection, cmd);
    	int cmdQueueLen = MongoDB.db().find(strCollection, Command.class).asList().size();
    	EventLogController.logEvent(p, "Befehl [" + cmd.command + "] erteilt (#" + cmdQueueLen +")", EventTypes.EVENT_TYPE_COMMAND_QUEUE_IN);
    	
	}
	
	public static void clearQueue(Project p) {
		
		// Get project and collectionName
		String strCollection = p.id.toHexString() + ".commands";

		// Drop collection
		MongoDB.db().getDB().getCollection(strCollection).drop();
		EventLogController.logEvent(p, "Warteschlange gelöscht.", EventTypes.EVENT_TYPE_COMMAND_QUEUE_DELETE);
	}
}
