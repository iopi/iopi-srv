package controllers;

import services.*;

import static controllers.ProjectController.publisher;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.*;
import play.libs.Json;
import play.mvc.*;

public class SensorDataController extends Controller {

	private static int colorIndex = 0;
	private static ArrayList<String> chartColors = new ArrayList<String>();
	
	public Result receiveData(String pName, String sName, double sValue) {
		
		// Get the project and collection name
		Project p = ProjectController.getProject(pName);
		String collectionName = p.id.toHexString()+".data."+sName;

		// Create the sensor data
		SensorData sensorData = new SensorData();
		sensorData.value = sValue;
		
		if(MongoDB.db().save(collectionName, sensorData) == null)
			return internalServerError("Saving SensorData to database failed.");
				
		if(!p.data_sets.contains(sName)) {
			p.data_sets.add(sName);
			p.update();
		}
			
		// Broadcast the data set
		broadCastChartDataSet(p, sName);
		
		return ok("OK");
	}
	
	public static void deleteDataSet(Project p, String sName) {
		
		// Get the project and collection name
		String strCollection = p.id.toHexString()+".data."+sName;
		
		// Drop collection
		MongoDB.db().getDB().getCollection(strCollection).drop();
		
		p.data_sets.remove(sName);
		p.update();
		
		EventLogController.logEvent(p, "Datensatz gelöscht: [" + sName + "]", EventTypes.EVENT_TYPE_DATASET_DELETE);
		
		// Broadcast updated data sets
		ObjectNode msg = Json.newObject();
		msg.put("type", "chart-data");
		msg.set("data", getJsonChartData(p));
		publisher.broadcast(p, msg);
	}
	
	public static void copyDataSet(Project p, String source, String target) {
		String sourceCollection = p.id.toHexString()+".data."+source;
		String targetCollection = p.id.toHexString()+".data."+target;
		
		List<SensorData> sourceData = MongoDB.db().find(sourceCollection, SensorData.class).asList();
		Iterator<SensorData> it = sourceData.iterator();
		while(it.hasNext()) {
			MongoDB.db().save(targetCollection, it.next());
		}
		p.data_sets.add(target);
		p.update();
		
		EventLogController.logEvent(p, "Datensatz kopiert: [" + source + "] > [" + target + "]", EventTypes.EVENT_TYPE_DATASET_DELETE);
		broadCastChartDataSet(p, target);
	}
	
	public static JsonNode getJsonChartData(Project p) {
		// Get the sensor data
		List<ObjectNode> chartData = new ArrayList<ObjectNode>();
		initChartColors();
		for(int i=0; i<p.data_sets.size(); i++) {
			String sName = p.data_sets.get(i);
			chartData.add( getChartDataSet(p, sName) );
		}												
		return Json.toJson(chartData);
	}
	
	private static ObjectNode getChartDataSet(Project p, String sName) {
		
		ObjectNode node = Json.newObject();
		node.put("label", sName);
		node.put("fill", false);
		node.put("lineTension", 0);
		node.put("hidden", false);
		
		String color = getNextChartColor();
		node.put("borderColor", color);
		node.put("backgroundColor", color);

		List<SensorData> dataSet = MongoDB.db().find(p.id.toHexString()+".data."+sName, SensorData.class).asList();
		node.set("data", Json.toJson(dataSet));
		
		return node;
	}
	
	private static void initChartColors() {
		
		if(chartColors.isEmpty()) {
			chartColors.add("rgb(68, 68, 68)");
			chartColors.add("rgb(75, 150, 75)");									
			chartColors.add("rgb(250, 120, 00)");
			chartColors.add("rgb(75, 150, 200)");
			chartColors.add("rgb(192, 120, 192)");			
		}
		colorIndex = 0;
	}
	
	private static String getNextChartColor() {
		
		while(colorIndex >= chartColors.size()) {
			int r = (int)(Math.random()*256);
			int g = (int)(Math.random()*256);
			int b = (int)(Math.random()*256);
			chartColors.add("rgb("+r+", "+g+", "+b+")");
		}
		return chartColors.get(colorIndex++);
	}
	
	private static void broadCastChartDataSet(Project p, String sName) {
		ObjectNode msg = Json.newObject();
		msg.put("event_type", "chartdata");
		
		ObjectNode data = Json.newObject();
		data.put("action", "update-set");
		msg.set("data", Json.newObject());
		msg.set("data", getChartDataSet(p, sName));
		publisher.broadcast(p, msg);
	}
}
