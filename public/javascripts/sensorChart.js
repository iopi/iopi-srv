
/**
 * The config object passed to the Chart.js object.
 */
var chartConfig = {
	type:"line",
	data:{
		datasets: [] // This will be provided by the controller.
	},
	options: {
		responsive: true,
        title:{
            display:false
        },
        tooltips: {
			bodyFontStyle: "bold",
           	footerFontStyle: "normal",
            callbacks: {
               	
               	title: function(tooltipItems, data) {
                   	return "";
                   },
           
                   footer: function(tooltipItems, data) {
                   	return moment(data.datasets[tooltipItems[0].datasetIndex].data[tooltipItems[0].index].x).format('D. MMM HH:mm:ss.SSS');
           		},
                   
           		label: function(tooltipItem, data) {
           			return data.datasets[tooltipItem.datasetIndex].label + ": " +
                   	data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].y;
           		}
               }
           },
			scales: {
				xAxes: [{
					type: "time",
					time: {
	                    unit: 'second',
	                    displayFormats: {
	                             second: 'D.MMM HH:mm'
	                    }
	                },
					display: true,
					weight: 50,
					scaleLabel: {
						display: true,
						labelString: 'Zeit'						
					},
	                ticks: {
	                	autoSkip: true,	    
	                	suggestedMin: 0,
						suggestedMax: 30
	                }
				}],
				yAxes: [{
					display: true,					
					scaleLabel: {
						display: true,
						labelString: 'Wert'
					},
					ticks: {
						suggestedMin: 0,
						suggestedMax: 30
					}
				}] 
			} //~ scales
	} //~ options
}; //~ chartConfig

/**
 * Object literal for the data view.
 */
var dataView = {

		// The data time interval 
		dataInterval: 0,
		
		// The sensor data and a backup to restore filters
		sensorData: [],
		sensorDataBackup: [],
		
		// The chart canvas and chart.js object
		chartCanvas: undefined,
		sensorChart: undefined,
		
		/**
		 * onReady
		 * -------
		 * Call this when the document is ready.
		 */
		onReady: function() {
			
			/* Click events */
			$("#deleteDataSet").click( dataView.showDeleteDataModal );
			$("#deleteDataSetSmall").click( dataView.showDeleteDataModal );			
			$(".dataInterval").children("input").click( dataView.updateDataInterval );
			$("#deleteDataModal").click( dataView.outsideModalClick );
			$("#confirmDeleteData").click( dataView.confirmDeleteDataSet );
			$("#cancelDeleteData").click( dataView.cancelDeleteDataSet );
			
			
			// Set up the data chart
			Chart.defaults.global.animation.duration = 0;
			dataView.sensorChart = new Chart($("#sensorChart").get(0), chartConfig);
			
			dataView.sensorDataBackup = JSON.parse( $("#sensorData").html() );
			$("#sensorData").remove();
			dataView.updateDataInterval()
		},
		
		/**
		 * updateChartData
		 * ---------------
		 * Updates and replaces the sensor data, including backup, with the given data. Filters are then
		 * applied to the sensor data. 
		 * @param data the new sensor data
		 */
		updateChartData: function(data) {
			dataView.sensorData = JSON.parse(JSON.stringify(data));
			dataView.sensorDataBackup = JSON.parse(JSON.stringify(data));
			
			chartConfig.data.datasets = dataView.sensorData;
			dataView.filterDataInterval();		
			
			if(dataView.sensorData.length == 0) {
				dataView.hideChart();
			
			} else {
				dataView.showChart();
				dataView.sensorChart.update();
			}
		},
		
		/**
		 * updateChartDataSet
		 * ------------------
		 * Updates a single data set or creates a new one if no set of the same label exists yet. Leaves
		 * all other data sets untouched.
		 * @param dataSet the data set to be updated
		 */
		updateChartDataSet: function(dataSet) {

			// See if a data set with that name is already present.
			// If so, replace it with the new one.
			var found = false;
			for(var i=0; i<chartConfig.data.datasets.length; i++) {
				
				if(chartConfig.data.datasets[i].label == dataSet.label) {
					
					dataSet.hidden = !dataView.sensorChart.isDatasetVisible(i);
					dataSet.borderColor = chartConfig.data.datasets[i].borderColor;
					dataSet.backgroundColor = chartConfig.data.datasets[i].backgroundColor;
					
					chartConfig.data.datasets[i] = dataSet;
					dataView.sensorDataBackup[i] = JSON.parse(JSON.stringify(dataSet));
					
					found = true;
				}
			}
			
			// If no such dataset exists, add it.
			if(!found) {
				
				chartConfig.data.datasets.push(dataSet);
				dataView.sensorDataBackup.push(JSON.parse(JSON.stringify(dataSet)));
				
				if(chartConfig.data.datasets.length == 1) {
					dataView.showChart();
					
				} else {
					$("#sensorSelect").append("<option>"+escapeHtml(dataSet.label)+"</option>");
				}
			}	
			
			// Update the chart.
			dataView.filterDataInterval(chartConfig.data.datasets);
			dataView.sensorChart.update();
		},
		
		/**
		 * dataSetExists
		 * -------------
		 * Checks if a dataset of the given name exists. Return [true] if a matching set is
		 * found. False, otherwise.
		 * @param setLabel the label of the dataset
		 */
		dataSetExists: function(setLabel) {
			var datasets = chartConfig.data.datasets;
			if(datasets == undefined || datasets.length == 0) 
				return false;
			
			for(var i=0; i<datasets.length; i++) {
				if(datasets[i].label == setLabel) return true;
			}
			return false;
		},
		
		/**
		 * updateDataInterval
		 * ------------------
		 * Updates dataView.dataInterval according to the currently selected UI option, 
		 * then applies the new filters to the displayed sensor data.
		 */
		updateDataInterval: function() {
			
			var selected = $(".sensorUI input:checked").attr("id");
			
			switch(selected) {
			case "dint30s":
				dataView.dataInterval = 30000;
				break;
			case "dint1m":
				dataView.dataInterval = 60000;
				break;
			case "dint5m":
				dataView.dataInterval = 300000;
				break;
			case "dintAll":
				dataView.dataInterval = 0;
				break;
			}
			
			for(var i=0; i<dataView.sensorData.length; i++) {
				dataView.sensorDataBackup[i].hidden = !dataView.sensorChart.isDatasetVisible(i);
			}
			
			dataView.updateChartData(dataView.sensorDataBackup);
		},
		
		/**
		 * filterDataInterval
		 * ------------------
		 * Applies time interval filter to the displayed sensor data according to the currently
		 * set dataView.dataInterval value.
		 */
		filterDataInterval: function() {
			if(dataView.dataInterval == 0) return;
			
			for(var i=0; i<dataView.sensorData.length; i++) {
				
				var threshold = dataView.sensorData[i].data[dataView.sensorData[i].data.length-1].x - dataView.dataInterval;
				
				for(var j=dataView.sensorData[i].data.length-1; j>=0; j--) {
					
					if(dataView.sensorData[i].data[j].x < threshold) {
						dataView.sensorData[i].data.splice(0,(j+1));
						break;
					}
				}
			}	
		},
		
		/**
		 * hideChart
		 * ---------
		 * Hides the data chart and shows a notification instead.
		 */
		hideChart: function() {
			$("#noData").show();
			$(".sensorUI").hide();
			$("#sensorChart").css("display", "none");
		},
		
		/**
		 * showChart
		 * ---------
		 * Shows the data chart and hides the notification.
		 */
		showChart: function() {
			$("#noData").hide();
			$(".sensorUI").show();
			$("#sensorChart").css("display", "block");
		},
		
		/**
		 * showDeleteDataModal
		 * -------------------
		 * Event listener callback showing the modal to delete a data set.
		 * @param event the event
		 */
		showDeleteDataModal: function(event) {
			event.preventDefault();
			
			$("#deleteDataRadioList").empty();
			
			var chartData = chartConfig.data.datasets;
			for(var i=0; i<chartData.length; i++) {
				$("#deleteDataRadioList").append("<label class='radioLabel'>"+chartData[i].label+
					"<input id='"+chartData[i].label+"' type='radio' name='deleteDataRadio'>" +
					"<span class='checkmark'></span>" +
					"</label>");	
			}
			$("#deleteDataRadioList").children().first().children("input").attr("checked", "checked");
			$("#deleteDataModal").css("display", "block");
		},
		
		/**
		 * outsideModalClick
		 * -----------------
		 * Event listener callback hiding the modal after a click outside its bounds.
		 * @param event the event
		 */
		outsideModalClick: function(event) {
			var modal = $("#deleteDataModal");
			if(event.target == modal.get(0)) 
				modal.css("display", "none");
		},
		
		/**
		 * confirmDeleteDataSet
		 * --------------------
		 * Event listener callback to delete the dataset selected via the modal. THe modal is then hidden.
		 * @param event the event
		 */
		confirmDeleteDataSet: function(event) {
			event.preventDefault();
			var selected = $(".modalBody input:checked").attr("id");
			projectView.ws.sendDeleteDataSet(selected);
			$("#deleteDataModal").css("display", "none");
		},
		
		/**
		 * cancelDeleteDataSet
		 * -------------------
		 * Event listener callback to cancel the delete dataset modal.
		 * @param event the event
		 */
		cancelDeleteDataSet: function(event) {
			$("#deleteDataModal").css("display", "none");
		}
};

$( document ).ready( dataView.onReady );