/**
 * 
 */

class WebSocketClient {
	constructor(wsUrl, pName) {
		
		this.wsUrl = wsUrl;
		this.pName = pName
		
		this.keepAlive = true;
		this.timeOut = undefined;
		this.heartBeat = undefined;
		
		this.initWebSocket();

    	if(this.keepAlive) {
    		this.heartBeat = setInterval(
    			this.sendPing, 4000, this);
    	}
	}
	
	initWebSocket() {
		var that = this;
		
		this.ws = new WebSocket(this.wsUrl);
		
		this.ws.onerror = function(error) {
	        console.log('WebSocket Error:');
	        console.log(error);
	    };
	    
	    this.ws.onopen = function(event) {
	    	console.log('Connected to web socket');
	    	projectView.connected();
	    };
	    
	    this.ws.onmessage = function(event) {
	        
	    	var message = JSON.parse(event.data);
	        
	        switch(message.event_type) {
	        
	        case "system":
		        that.systemEvent(message.data);
	        	break;
	        
	        case "user":
	        	that.userEvent(message.data);
	        	break;
	        	
	        case "chartdata":
	        	that.datasetEvent(message.data);
	        	break;
	        
	        case "eventlog":
	        	that.logEvent(message.data);
	        	break;
	        	
        	default:
        		console.log("Invalid web socket message:");
        		console.log(message);
	        }
	    };
	    
	    this.ws.onclose = function(event) {
	        console.log("Disconnected from WebSocket.");
	        projectView.disconnected();
	    };	
	}
	
	
	/**
	 * SENDING MESSAGES TO THE SERVER
	 * ==============================
	 */
	sendPing(that) {
		
		console.log("Trying to send ping with ready state: ", that.ws.readyState);
		
		switch(that.ws.readyState) {
			case WebSocket.OPEN:
				console.log("ping")
				that.ws.send(JSON.stringify({
					event_type: 	"system",
					data: {
						action: 	"ping",
						uid:		projectView.uid
					}
				}));
				
				if(that.timeOut === undefined) {
					that.timeOut = setTimeout(() => {
						console.log("Websocket ping pong timeout.");
					//	that.ws.close();	
						that.timeOut = undefined;
					}, 4000);
				}
				
				break;
				
			case WebSocket.CLOSED:
				console.log("Websocket closed, trying to reconnect...");
				that.initWebSocket();
				break;
		}
			
		
	}
	
	sendCommand(cmdText) {
		this.ws.send(JSON.stringify({
			event_type: 	"command",
			data: {
				action:		"issue",
				project: 	this.pName,
				command:	cmdText
			}
		}));
	}
	
	sendClearQueue() {
		this.ws.send(JSON.stringify({
			event_type: 	"command",
			data: {
				action: 	"clear_queue",
				project:	this.pName
			}
		}));
	}
	
	sendDeleteDataSet(sName) {
		this.ws.send(JSON.stringify({
			event_type: 	"chartdata",
			data: {
				action: 	"delete_set",
				project:	this.pName,
				setname:	sName
			}
		}));
	}
	
	/**
	 * PROCESSING MESSAGES FROM THE SERVER
	 * ===================================
	 */
	systemEvent(data) {
		switch(data.action) {
			case "ping":
				this.ws.send(JSON.stringify({
					event_type: "system",
					data: {
						action: "pong"
					}
				}));
		
			case "pong":
				console.log("pong");
				clearTimeout(this.timeOut);
				this.timeOut = undefined;
				break;
				
			default:
	        	console.log("Received unspecified system action: " + data.action);
		}
	}
	
	userEvent(data) {
		switch(data.action) {
		
		case "joined":
		case "left":
			projectView.updateActiveUsers(data.number);
        	break;
        
        default:
        	console.log("Received unspecified user action: " + data.action);
		}
	}
	
	logEvent(data) {
		eventView.addEventLog(data, true);
    	eventView.filterEvents();
    	$('#eventList').scrollTop( $('#eventList')[0].scrollHeight );
	}
	
	datasetEvent(data) {
		switch(data.action) {
			case "update_chart":
				dataView.updateChartData(data.chartdata);
				break;
				
			case "update_set":
				dataView.updateChartDataSet(data.dataset);
				break;
				
			default:
	        	console.log("Received unspecified dataset action: " + data.action);
		}
	}
}