/**
 * A simple WebSocket client
 */

var projectView = {
		ws: undefined,
		uid: guid(),
		
		onReady: function() {
			
			var wsUrl = $("#socketURL").html();
			var pName = $("title").html();
			
			console.log(wsUrl);
			console.log(pName);
			
			projectView.ws = new WebSocketClient(wsUrl+projectView.uid, pName);
			
			console.log($("#socketURL") );
			$("#socketURL").remove();
			console.log($("#socketURL") );
			
			console.log(projectView.uid);
		},
		
		updateActiveUsers: function(numUsers) {
			$("#numUsers").text(numUsers);
			blinkInfo($("#users"));
		},
		
		connected: function() {
			$(".socketed").css("background-color", "");
			$(".socketed").css("cursor", "");
			$(".socketed").removeAttr("disabled");
		},
		
		disconnected: function() {
			$(".socketed").css("background-color", "var(--inactive)");
			$(".socketed").css("cursor", "not-allowed");
			$(".socketed").attr("disabled", "disabled");
			
			$("#numUsers").empty();
			$("#numUsers").append("<i class='fa fa-spinner fa-pulse'></i>");
		},
		
		wsConnected: function() {
			if(projectView.ws == undefined) return false;
			if(projectView.ws.ws == undefined) return false;
			return (projectView.ws.ws.readyState == WebSocket.OPEN);
		}
};

$( document ).ready( projectView.onReady );
