/**
 * 
 */



var entityMap = {
	  '&': '&amp;',
	  '<': '&lt;',
	  '>': '&gt;',
	  '"': '&quot;',
	  "'": '&#39;',
	  '/': '&#x2F;',
	  '`': '&#x60;',
	  '=': '&#x3D;'
};

function escapeHtml (string) {
	return String(string).replace(/[&<>"'`=\/]/g, function (s) {
		return entityMap[s];
	});
}

function blink(jQElement, color, time) {
	jQElement.css("background-color", color);
	
	var t = setTimeout(function () {
    	jQElement.css("background-color", "");
    },
     time);
}

function blinkError(jQElement) {
	blink(jQElement, "#ffa1a1", 550);
}

function blinkInfo(jQElement) {
	blink(jQElement, "var(--highlight)", 1000);
}

function guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	  }
	
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
