/**
 * 
 */
var aboutView = {
		
		onReady: function() {
			var stats = JSON.parse( $("#jsonStats").html() );
			
			$("#srvVersion").append(stats.version);
			$("#srvStartDate").html(stats.startdate);
			$("#srvStartTime").html(stats.starttime);
			$("#numProjects").html(stats.projects);
			
			if(stats.imprint != undefined) {
				$(".footer").html("<a href='"+stats.imprint+"'>Impressum</a>");
			}
		}
};

$( document ).ready( aboutView.onReady );
