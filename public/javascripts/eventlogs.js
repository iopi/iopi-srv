
var eventView = {
	
	/**
	 * onReady
	 * -------
	 * Call this when the document is ready.
	 */	
	onReady: function() {
		$("#eventFilter").keyup( eventView.filterEvents );
		
		var eventLogs = JSON.parse( $("#jsonEvents").html() );
		$("#jsonEvents").remove();
		eventView.buildEventLogTable(eventLogs);
	},
	
	/**
	 * buildEventLogTable
	 * ------------------
	 */
	buildEventLogTable: function(events) {
		for(var i=0; i<events.length; i++) {
			eventView.addEventLog(events[i], false);
		}
		eventView.filterEvents();		
	},
	
	/**
	 * addEventLog
	 * -----------
	 */
	addEventLog: function(el, blink) {
		// Create the new table row
		
		var eventRow = $("#eventList").append("<tr></tr>").children().last();
		
		switch(el.event_type) {
		case "EVENT_TYPE_USER":		
			eventRow.append("<td class='eventType'><i class='fa fa-commenting-o icon-flipped'></i></td>");
			break;
		case "EVENT_TYPE_SYSTEM":
			eventRow.append("<td class='eventType'><i class='fa fa-star'></i></td>");
			break;
		case "EVENT_TYPE_COMMAND_QUEUE_IN":
			eventRow.append("<td class='eventType'><i class='fa fa-sign-out'></i></td>");
			break;
		case "EVENT_TYPE_COMMAND_QUEUE_OUT":
			eventRow.append("<td class='eventType'><i class='fa fa-sign-in'></i></td>");
			break;
		case "EVENT_TYPE_COMMAND_QUEUE_DELETE":
		case "EVENT_TYPE_DATASET_DELETE":
			eventRow.append("<td class='eventType'><i class='fa fa-trash-o'></td>");
			break;
		default:
			eventRow.append("<td class='eventType'></td>");
		}
		eventRow.append('<td>'+escapeHtml(el.log_message)+'</td>');
		
		if(blink) blinkInfo(eventRow);
	},
	
	/**
	 * filterEvents
	 * ------------
	 */
	filterEvents: function() {
		var	filter,						// only display events that match the filter query		
		tableContent, tr, td, 		// table content
		i, j, display;				// support variables

		// Get the filter query and type information
		filter 		= document.getElementById("eventFilter").value.toUpperCase();

		// Get the table content
		tableContent = document.getElementById("eventList");
		tr = tableContent.getElementsByTagName("tr");

		// Loop through table rows
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td");
		
			// Hide rows whose content does not match the filter query
			display = (td[1].innerHTML.toUpperCase().indexOf(filter) > -1);
		
			// Set display property
			if(display)	tr[i].style.display = "";
			else		tr[i].style.display = "none";
		}
		$("#eventList").scrollTop( $("#eventList")[0].scrollHeight );
	}
};

/**
 * Populates the event log table with the given elements, then filters the events and
 * sets the table scroll position to the bottom.
 * @param events an array of event JSONs
 */


/**
 * Function that adds an event to the event log table
 * @param el the event as JSON
 */


/**
 * Function that filters the events in the event log table and only shows those that match
 * the filter query and one of the selected event types.
 */


$( document ).ready( eventView.onReady );