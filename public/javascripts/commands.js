/**
 * 
 */

var cmdView = {
		
		onReady: function() {

			// Event listener callbacks
			$("#sendCmd").click( cmdView.sendCmd );
			$("#sendCmdSmall").click( cmdView.sendCmd );
			
			$("#deleteCmdQueue").click( cmdView.showDeleteQueueModal );
			$("#deleteCmdQueueSmall").click( cmdView.showDeleteQueueModal );

			$("#confirmDeleteQueue").click( cmdView.confirmDeleteQueue );
			$("#cancelDeleteQueue").click( cmdView.cancelDeleteQueue );
			
			$("#deleteCommandQueueModal").click( cmdView.outsideModalClick );
		},
		
		/**
		 * sendCmd
		 * -------
		 */
		sendCmd: function(event) {
			event.preventDefault();
			
			var cmdText = $("#cmdText").val();
			if(cmdText != '') {
				projectView.ws.sendCommand(cmdText);
				$("#cmdText").val('');
			} else {
				blinkError( $("#cmdText") );
			}
		},
		
		/**
		 * showDeleteQueueModal
		 * --------------------
		 */
		showDeleteQueueModal: function(event) {
			event.preventDefault();
			$("#deleteCommandQueueModal").css("display", "block");
		},
		
		/**
		 * confirmDeleteQueue
		 * ------------------
		 */
		confirmDeleteQueue: function(event) {
			event.preventDefault();
			projectView.ws.sendClearQueue();
			$("#deleteCommandQueueModal").css("display", "none");
		},
		
		/**
		 * cancelDeleteQueue
		 * -----------------
		 */
		cancelDeleteQueue: function(event) {
			$("#deleteCommandQueueModal").css("display", "none");
		},
		
		outsideModalClick: function(event) {
			var modal = $("#deleteCommandQueueModal");
			if(event.target == modal.get(0)) 
				modal.css("display", "none");
		}
				
}

$( document ).ready( cmdView.onReady );