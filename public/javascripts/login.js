/**
 * 
 */

var loginView = {
	
	onReady: function() {
		$("#login").click( loginView.checkProjectName );
	},
	
	checkProjectName: function(event) {
		var regex = new RegExp("[^A-Za-z0-9_ ]");
    	var pName = $("input[name=pName]").val();
    	if(pName == "" | regex.test(pName)) {
    		event.preventDefault();
    		blinkError( $("input[name=pName]") );
    	}
	}
};

$( document ).ready( loginView.onReady );