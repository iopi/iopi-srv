# Internet of Pi - Server

Der IoPi-Server ist ein IoT-Server für den schulischen bzw. didaktischen Einsatz im Kontext des Internet der Dinge. Er ist darauf ausgelegt mit Client-Geräten (z.B einem Arduino oder Raspberry Pi - daher der Name "Internet of Pi") zu kommunizieren: Daten, Nachrichten oder Befehle mit ihnen auszutauschen. Zudem stellt der Server ein entpsrechendes Web-Interface zur Verfügung, über das die Daten, und Nachrichten eingesehen bzw. Befehle verschickt werden können.

## Installation

Eine aktuelle Instanz des IoPi-Servers ist unter https://iopi.informatik.hu-berlin.de öffentlich verfügbar. Sollten die verwendeten Endgeräte (z.B aus dem Schulnetzwerk heraus) keinen Internetzugang haben, kann auch ein eigener Server im lokalen Netz aufgesetzt werden. Wie dies geht, ist in der Datei [INSTALLATION.md](INSTALLATION.md) genauer beschrieben.

## Verwendung

Die Kommunikation mit dem Server geschieht einerseits mit einem (mobilen) Browser und die darüber angezeigte Web-Oberfläche, und andererseits mit IoT-Geräten (Arduino, Raspi etc) über speziell dafür vorgesehene Schnittstellen.

### Web-Oberfläche

Die Web-Oberfläche ist, sofern nicht anders konfiguriert, unter Port 80 der Root-URL des Servers erreichbar. Für den "Log in" auf der Startseite ist lediglich die Angabe eines Projektnamens erforderlich. Dieser kann frei gewählt werden. Wenn noch kein Projekt des entpsrechenden Namens existiert, wird es automatisch generiert.

Die daraufhin angezeigte Projektseite ist in drei Abschnitte unterteilt:

1. **Ereignisanzeige**: Hier werden verschiedene Statusmeldungen gelistet, z.B das Versenden und Empfangen von Befehlen und Nachrichten oder das Löschen von Datensätzen.

2. **Befehle**: Hier können Befehle an IoT-Geräte erteilt werden. Diese werden vom Server zunächst in einer Warteschlange vorgehalten bis sie von einem Gerät "abgeholt" werden. Siehe dazu die GET Command-Schnittstelle unten. 

3. **Sensordaten**: Hier werden Sensordaten, die der Server von IoT-Geräten erhalten hat, grafisch dargestellt.

### IoT-Schnittstellen

Die Kommunikation über die IoT-Schnittstellen erfolgt über einfache HTTP-Requests. Prinzipiell können sie somit von jedem HTTP-fähigen Endgerät angesprochen werden. Voraussetzung ist, dass das Gerät mit einem Netzwerk verbunden ist, über das es den Server erreichen kann.

Am einfachsten lässt sich der Server mithilfe der IoPi-Bibliothek für die Arduino IDE ansprechen (https://gitlab.informatik.hu-berlin.de/iopi/iopi-lib). Die Bibliothek setzt auf der HttpClient-Bibliothek auf, die für nahezu alle Arduino-Plattformen (und auch andere Geräte) implementiert ist. 

Der IoPi-Server Stellt drei verschiedene HTTP-Schnittstellen für IoT-Geräte zur verfügung:

1. **POST Nachricht**  
	Über diese Schnittstelle kann eine Nachricht an den Server geschickt werden, welche dann in der Ereignisanzeige des entsprechenden Projektes angezeigt wird. Die Anfrage benutzt die POST-Methode und hat das folgende Format: `<ADRESSE>/msg/<PROJEKT>/<NACHRICHT>`

	**Beispiel:** Die POST-Anfrage `http://iopi.informatik.hu-berlin.de/msg/Mein-Projekt/Hallo`	schickt die Nachricht "Hallo" and das Projekt "Mein-Projekt".

2. **POST Data**  
	Über diese Route können Daten, z.B. von Sensoren, an den Server geschickt werden und zu einem Datensatz aggregiert werden. Die Daten werden unter "Sensordaten" im Web-Interface des entpsrechenden Projektes angezeigt. Die Anfrage benutzt die POST-Methode und hat das folgende Format: `<ADRESSE>/dat/<PROJEKT>/<LABEL>/<WERT>`  

	**Beispiel:** Die POST-Anfrage `http://iopi.informatik.hu-berlin.de/dat/Mein-Projekt/Temperatur/22` schickt den Wert 22 unter dem Namen "Temperatur" an das Projekt "Mein-Projekt". Alle weiteren Werte, die ebenfalls unter dem Namen "Temperatur" an dieses Projekt verschickt werden, werden dem selben Datensatz zugeordnet und im Abschnitt "Sensordaten" der Web-Oberfläche als ein zusammenhängender Graph dargestellt.

3. **GET Command**  
	Über diese Route kann der Server gefragt werden, ob ein Befehl für das angegebene Projekt vorliegt. Wenn ja, schickt der Server den entsprechenden Befehl als Antwort zurück. Wenn nein, schickt der Server einen leeren String. Die Anfrage benutzt die GET-Methode und hat das folgende Format: `<ADRESSE>/cmd/<PROJEKT>/`

	**Beispiel:** Die GET-Anfrage `http://iopi.informatik.hu-berlin.de/cmd/Mein-Projekt` fragt den Server, ob ein Befehl für das Projekt "Mein-Projekt" vorliegt. Falls ja, gibt der Server den nächsten Befehl in der Befehlswarteschlange als einfachen String als Antwort zurück. Feil nein, antwortet der Server mit dem leeren String.